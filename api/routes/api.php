<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

     // public routes
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/register', 'Api\AuthController@register')->name('register.api');
    Route::get('/posts','Api\PostController@getPosts')->name('posts.api');
    Route::get('/posts/{slug}','Api\PostController@getPost')->name('post.api');
    Route::get('/posts/{slug}/comments','Api\PostController@getComments')->name('post.comments');

    // private routes
    Route::middleware('auth:api')->group(function () {
        Route::post('/logout', 'Api\AuthController@logout')->name('logout');
        Route::post('/posts', 'Api\PostController@createPost');
        Route::patch('/posts/{slug}','Api\PostController@updatePost')->name('post.update');
        Route::delete('/posts/{slug}','Api\PostController@deletePost')->name('post.delete');
        Route::post('/posts/{slug}/comments','Api\PostController@addComment')->name('post.addComment');
        Route::patch('/posts/{slug}/comments/{id}','Api\PostController@editComment')->name('post.editComment');
        Route::delete('/posts/{slug}/comments/{id}','Api\PostController@deleteComment')->name('post.deleteComment');
    });

});

