<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Posts;
use App\Comments;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function getPosts() {
        $posts = Posts::paginate(15);
        $response = [
            "data" => $posts->items(),
            "links" => [
                "first" => $posts->url(1),
                "last" => $posts->lastPage(),
                "prev" => $posts->previousPageUrl(),
                "next" => $posts->nextPageUrl(),
            ],
            "meta" => [
                "current_page" => $posts->currentPage(),
                "from" => $posts->firstItem(),
                "last_page" => $posts->lastPage(),
                "path" => $posts->getOptions()['path'],
                "to" => $posts->count(),
                "total" => $posts->total(),
            ]
        ];

        return response($response,200);
    }

    public function getPost($slug="") {
        $post = Posts::where('slug',$slug)->first();

        if($post) {
            return response(['data' => $post],200);
        }
        $response = ['message' => "No query results found for model ".class_basename(Posts::class)];

        return response($response, 404);
    }

    public function createPost(Request $request) {
        $validator = Validator::make($request->all(),[
            'title' => 'bail|required|string|min:5|max:50',
            'content' => 'bail|required|string|min:10|max:500',
            'slug' => 'bail|required|min:5|max:50'
        ]);

        if($validator->fails()) {
            return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
        }

        $request['user_id'] = $request->user()->id;

        $response = Posts::create($request->toArray());

        return response($response,200);
    }

    public function updatePost($slug="", Request $request) {
        $validator = Validator::make($request->all(),[
            'title' => 'bail|required|string|min:5|max:50',
            'content' => 'bail|required|string|min:10|max:500',
            'slug' => 'bail|required|min:5|max:50'
        ]);

         if($validator->fails()) {
            return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
        }

        $response = Posts::where('slug', $slug)
                    ->update($request->toArray());

        if($response) {
            return response(['title' => $request->title],200);
        }

        return response([],200);
    }

    public function deletePost($slug = "") {
        $deleted = Posts::where('slug',$slug)
                        ->delete();

        if($deleted) {
            return response(['status' => 'record deleted sucessfully'], 200);
        }

        return response(['status' => 'record deleted unsuccessfully'], 422);
    }

    public function addComment($slug="", Request $request) {
        $validator = Validator::make($request->all(),[
            "body" => 'bail|required|min:5|max:100|string'
        ]);

        if($validator->fails()) {
             return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
            };

        $request['creator_id'] = $request->user()->id;
        $comment = new Comments($request->toArray());

        $post = Posts::where('slug',$slug)->firstOrFail(['id']);

        if($post) {
            $response = $post->comments()->save($comment);

            return response($response,201);
        }

        return response($post,404);

    }

    public function editComment($slug="",$id=0, Request $request) {
         $validator = Validator::make($request->all(),[
            "body" => 'bail|required|min:5|max:100|string'
        ]);

        if($validator->fails()) {
             return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
        };

        $post = Posts::where('slug',$slug)->first(['id']);

        $response = $post->comments()->find($id)->update($request->toArray());

        if($response) {
            return response([$request->body],200);
        }

        return response([],200);

    }

    public function deleteComment($slug="",$id=0) {
        $post = Posts::where('slug',$slug)->first(['id']);

        $deleted = $post->comments()->find($id)->delete();

        if($deleted) {
            return response(['status' => 'record deleted successfully'], 200);
        }

        return response(['status' => 'record deleted unsuccessfully'], 422);
    }

    public function getComments($slug="") {
        $post =  Posts::where('slug',$slug)->first(['id']);

        return response(['data' => $post->comments()->get()],200);
    }

}
