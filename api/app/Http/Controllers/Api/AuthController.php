<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function register (Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails())
        {
            return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
        }

        $request['password']=Hash::make($request['password']);
        $user = User::create($request->toArray());

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];

        return response($response, 200);

    }

    public function login (Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
             return response([
                'message' => "The given data was invalid",
                'errors'=>$validator->errors()], 422);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {

            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client');
                $response = ['token' => $token->accessToken,
                             'token_type' => 'bearer',
                             'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                            ];
                return response($response, 200);
            } else {
                 $response = [
                    'message' => 'The given data was invalid',
                    'errors' => [
                        'email' => "These credentials do not match our records."
                    ]
                ];
                return response($response, 422);
            }

        } else {
            $response = [
                            'message' => 'The given data was invalid',
                            'errors' => [
                                'email' => "These credentials do not match our records."
                            ]
                        ];
            return response($response, 422);
        }

    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);

    }
}
